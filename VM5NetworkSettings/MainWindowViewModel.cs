﻿using Reactive.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VM5NetworkSettings
{
    class MainWindowViewModel
    {
        public ReactiveProperty<bool> UseTCP { get; private set; }
        public ReactiveProperty<bool> UseWebProxy { get; private set; }

        public ReadOnlyReactiveProperty<bool> WebProxyPanelIsEnabled { get; private set; }

        public ReactiveProperty<bool> UseOSProxySettings { get; private set; }
        public ReactiveProperty<bool> ProxySettingsAutoDetect { get; private set; }
        public ReactiveProperty<bool> UseProxyConfigScript { get; private set; }
        public ReactiveProperty<bool> UseSpecificProxyServer { get; private set; }

        public ReadOnlyReactiveProperty<bool> ProxyConfigScriptPanelIsEnabled { get; private set; }
        public ReadOnlyReactiveProperty<bool> SpecificProxyServerPanelIsEnabled { get; private set; }

        public ReactiveProperty<bool> UseProxyCredencials { get; private set; }
        public ReadOnlyReactiveProperty<bool> ProxyCredencialsPanelIsEnabled { get; private set; }

        public MainWindowViewModel()
        {
            UseTCP = new ReactiveProperty<bool>(false);
            UseWebProxy = new ReactiveProperty<bool>(false);

            WebProxyPanelIsEnabled = UseWebProxy.ToReadOnlyReactiveProperty();

            UseOSProxySettings = new ReactiveProperty<bool>(false);
            ProxySettingsAutoDetect = new ReactiveProperty<bool>(false);
            UseProxyConfigScript = new ReactiveProperty<bool>(false);
            UseSpecificProxyServer = new ReactiveProperty<bool>(false);

            ProxyConfigScriptPanelIsEnabled = UseProxyConfigScript.ToReadOnlyReactiveProperty();
            SpecificProxyServerPanelIsEnabled = UseSpecificProxyServer.ToReadOnlyReactiveProperty();

            UseProxyCredencials = new ReactiveProperty<bool>(false);
            ProxyCredencialsPanelIsEnabled = UseProxyCredencials.ToReadOnlyReactiveProperty();
        }
    }
}
